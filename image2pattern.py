#!/usr/bin/python

# image2pattern: CONVERT IMAGE TO PATTERN
# Copyright 2019  Lionel Maes
# lionel at lavillahermosa dot com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

from PIL import Image
import sys
import os

if len(sys.argv) < 5:
    print 'Usage: %s image.png maxwidthofpattern maxheightofpattern numberofcolors' % sys.argv[0]
    sys.exit()

imgfile = sys.argv[1]

pW = int(sys.argv[2])
pH = int(sys.argv[3])

colorsnb = int(sys.argv[4])

pSize = pW, pH

theImage = Image.open(imgfile).convert('L')

#theImage.load()

theImage.thumbnail(pSize, Image.ANTIALIAS)

theImage = theImage.quantize(colorsnb)

filename = os.path.splitext(imgfile)[0]

theImage.save('%s-pattern.png' % filename, "PNG")

print("pattern size : %s %s " % (theImage.size[0], theImage.size[1]))
